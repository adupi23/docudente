﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Odbc;

namespace DocuDente
{
    public partial class Form2 : Form
    {
        private readonly Form1 Form1;
        static Form2 _Form2;
        Paziente pazienteScelto;
        int delay = 200;
        bool updateEnablade = true;
        bool ValueChange = false;

        Dictionary<String, Object> values = new Dictionary<String, Object>();

        public Form2(Form1 _form1)
        {
            Form1 = _form1;
            _Form2 = this;
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            //head
            pazienteScelto = Form1.p;
            txtNomeP.Text = pazienteScelto.Nome;
            txtCognomeP.Text = pazienteScelto.Cognome;
            txtTelefonoP.Text = pazienteScelto.Telefono;
            datePickerUltimaVisita.Value = pazienteScelto.UltimaVisita;
            cBoxUltimaSeduta.Checked = pazienteScelto.UltimaSedutaIgene;
            datePickerUltimaSeduta.Value = pazienteScelto.UltimaSedutaIgeneData;
            if (cBoxUltimaSeduta.Checked)
            {
                datePickerUltimaSeduta.Enabled = true;
            }
            else {
                datePickerUltimaSeduta.Value = DateTime.Today;
            }
            //--head

            //foot
            cBoxOpt.Checked = pazienteScelto.Opt;
            datePickerOpt.Value = pazienteScelto.OptData;
            if (cBoxOpt.Checked)
            {
                datePickerOpt.Enabled = true;
            }
            else
            {
                datePickerOpt.Value = DateTime.Today;
            }
            cBoxTc.Checked = pazienteScelto.Tc;
            datePickerTc.Value = pazienteScelto.TcData;
            if (cBoxTc.Checked)
            {
                datePickerTc.Enabled = true;
            }
            else
            {
                datePickerTc.Value = DateTime.Today;
            }
            cBoxPlacca.Checked = pazienteScelto.Placca;
            datePickerPlacca.Value = pazienteScelto.PlaccaData;
            if (cBoxPlacca.Checked)
            {
                datePickerPlacca.Enabled = true;
            }
            else
            {
                datePickerPlacca.Value = DateTime.Today;
            }
            ValueChange = true;
            //--foot
        }

        TextBox tb = null;
        CheckBox cb = null;
        DateTimePicker dtp = null;

        private void updatePaziente(Dictionary<String, Object> values, Object input) {
            tb = null;
            cb = null;
            dtp = null;
            String forText = "'";
            if (input is TextBox)
            {
                tb = (TextBox)input;
            }
            if (input is CheckBox) {
                cb = (CheckBox)input;
                forText = "";
            }
            if (input is DateTimePicker)
            {
                dtp = (DateTimePicker)input;
            }
            OdbcConnection conn = Form1.connection;
            if (conn != null)
            {
                try
                {
                    String query = "Update Paziente Set " + values.First().Key + " = " + forText + values.First().Value.ToString() + 
                        forText + ", ultima_modifica = '" + DateTime.Now.ToString() + "' where ID = " + pazienteScelto.Id;
                    if (conn.State == ConnectionState.Open)
                    {
                        conn.Close();
                    }
                    conn.Open();
                    OdbcCommand update = new OdbcCommand(query, conn);
                    update.ExecuteNonQuery();
                    enableInputs();
                    conn.Close();
                }
                catch (Exception ex) {
                    conn.Close();
                    enableInputs();
                    MessageBox.Show("ERRORE UPDATE: \r\n" + ex.Message);
                }
            }
            else {
                enableInputs();
                MessageBox.Show("Connessione Fallita");
            }
            values.Clear();
        }

        private void enableInputs() {
            if (tb != null)
            {
                tb.Enabled = true;
            }
            if (cb != null)
            {
                cb.Enabled = true;
            }
            if (dtp != null)
            {
                dtp.Enabled = true;
            }
        }

        //head
        private void txtNomeP_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) {
                txtNomeP.Enabled = false;
                values.Add("nome", txtNomeP.Text);
                updatePaziente(values, sender);

                _Form2.Text = txtNomeP.Text + " " + txtCognomeP.Text;
            }
        }

        private void txtCognomeP_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtCognomeP.Enabled = false;
                values.Add("cognome", txtCognomeP.Text);
                updatePaziente(values, sender);
            }
            _Form2.Text = txtNomeP.Text + " " + txtCognomeP.Text;
        }

        private void txtTelefonoP_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtTelefonoP.Enabled = false;
                values.Add("telefono", txtTelefonoP.Text);
                updatePaziente(values, sender);
            }
        }

        private void datePickerUltimaVisita_ValueChanged(object sender, EventArgs e)
        {
            if (ValueChange) {
                datePickerUltimaVisita.Enabled = false;
                values.Add("ultima_visita", datePickerUltimaVisita.Value);
                updatePaziente(values, sender);
            }
        }

        private void cBoxUltimaSeduta_CheckedChanged(object sender, EventArgs e)
        {
            if (ValueChange)
            {
                cBoxUltimaSeduta.Enabled = false;
                if (cBoxUltimaSeduta.Checked)
                {
                    datePickerUltimaSeduta.Enabled = true;
                }
                else
                {
                    datePickerUltimaSeduta.Enabled = false;
                }
                values.Add("ultima_seduta_igene", cBoxUltimaSeduta.Checked);
                updatePaziente(values, sender);
                if (cBoxUltimaSeduta.Checked) {
                    values.Add("ultima_seduta_igene_data", datePickerUltimaSeduta.Value);
                    updatePaziente(values, datePickerUltimaSeduta);
                }
            }
        }

        private void datePickerUltimaSeduta_ValueChanged(object sender, EventArgs e)
        {
            if (ValueChange)
            {
                datePickerUltimaSeduta.Enabled = false;
                values.Add("ultima_seduta_igene_data", datePickerUltimaSeduta.Value);
                updatePaziente(values, sender);
            }
        }
        //--head

        //foot
        private void cBoxOpt_CheckedChanged(object sender, EventArgs e)
        {
            if (ValueChange) {
                cBoxOpt.Enabled = false;
                if (cBoxOpt.Checked)
                {
                    datePickerOpt.Enabled = true;
                }
                else
                {
                    datePickerOpt.Enabled = false;
                }
                values.Add("opt", cBoxOpt.Checked);
                updatePaziente(values, sender);
                if (cBoxOpt.Checked) {
                    values.Add("opt_data", datePickerOpt.Value);
                    updatePaziente(values, datePickerOpt);
                }
            }
        }

        private void datePickerOpt_ValueChanged(object sender, EventArgs e)
        {
            if (ValueChange)
            {
                datePickerOpt.Enabled = false;
                values.Add("opt_data", datePickerOpt.Value);
                updatePaziente(values, sender);
            }
        }

        private void cBoxTc_CheckedChanged(object sender, EventArgs e)
        {
            if (ValueChange)
            {
                cBoxTc.Enabled = false;
                if (cBoxTc.Checked)
                {
                    datePickerTc.Enabled = true;
                }
                else
                {
                    datePickerTc.Enabled = false;
                }
                values.Add("tc", cBoxTc.Checked);
                updatePaziente(values, sender);
                if (cBoxOpt.Checked)
                {
                    values.Add("tc_data", datePickerTc.Value);
                    updatePaziente(values, datePickerTc);
                }
            }
        }

        private void datePickerTc_ValueChanged(object sender, EventArgs e)
        {
            if (ValueChange)
            {
                datePickerTc.Enabled = false;
                values.Add("tc_data", datePickerTc.Value);
                updatePaziente(values, sender);
            }
        }

        private void cBoxPlacca_CheckedChanged(object sender, EventArgs e)
        {
            if (ValueChange)
            {
                cBoxPlacca.Enabled = false;
                if (cBoxPlacca.Checked)
                {
                    datePickerPlacca.Enabled = true;
                }
                else
                {
                    datePickerPlacca.Enabled = false;
                }
                values.Add("placca", cBoxPlacca.Checked);
                updatePaziente(values, sender);
                if (cBoxPlacca.Checked)
                {
                    values.Add("placca_data", datePickerPlacca.Value);
                    updatePaziente(values, datePickerPlacca);
                }
            }
        }

        private void datePickerPlacca_ValueChanged(object sender, EventArgs e)
        {
            if (ValueChange)
            {
                datePickerPlacca.Enabled = false;
                values.Add("placca_data", datePickerPlacca.Value);
                updatePaziente(values, sender);
            }
        }

        private void Form2_FormClosed(object sender, FormClosedEventArgs e)
        {
            Form1.fillTable(Form1.connection,"", "");
        }
        //--foot
    }
}
