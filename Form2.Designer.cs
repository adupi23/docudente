﻿namespace DocuDente
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.label1 = new System.Windows.Forms.Label();
            this.txtNomeP = new System.Windows.Forms.TextBox();
            this.txtCognomeP = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtTelefonoP = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.datePickerUltimaVisita = new System.Windows.Forms.DateTimePicker();
            this.datePickerUltimaSeduta = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.cBoxUltimaSeduta = new System.Windows.Forms.CheckBox();
            this.cBoxOpt = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.datePickerOpt = new System.Windows.Forms.DateTimePicker();
            this.datePickerTc = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.cBoxTc = new System.Windows.Forms.CheckBox();
            this.datePickerPlacca = new System.Windows.Forms.DateTimePicker();
            this.label8 = new System.Windows.Forms.Label();
            this.cBoxPlacca = new System.Windows.Forms.CheckBox();
            this.helpProvider1 = new System.Windows.Forms.HelpProvider();
            this.tabControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(11, 100);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(796, 307);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(788, 281);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "CARTELLA";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(788, 281);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "ToDo";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tabPage3
            // 
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(788, 281);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Storico";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Nome";
            // 
            // txtNomeP
            // 
            this.txtNomeP.Location = new System.Drawing.Point(53, 35);
            this.txtNomeP.Name = "txtNomeP";
            this.txtNomeP.Size = new System.Drawing.Size(100, 20);
            this.txtNomeP.TabIndex = 2;
            this.txtNomeP.Tag = "0";
            this.txtNomeP.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNomeP_KeyDown);
            // 
            // txtCognomeP
            // 
            this.txtCognomeP.Location = new System.Drawing.Point(217, 35);
            this.txtCognomeP.Name = "txtCognomeP";
            this.txtCognomeP.Size = new System.Drawing.Size(100, 20);
            this.txtCognomeP.TabIndex = 4;
            this.txtCognomeP.Tag = "1";
            this.txtCognomeP.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCognomeP_KeyDown);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(159, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Cognome";
            // 
            // txtTelefonoP
            // 
            this.txtTelefonoP.Location = new System.Drawing.Point(378, 35);
            this.txtTelefonoP.Name = "txtTelefonoP";
            this.txtTelefonoP.Size = new System.Drawing.Size(100, 20);
            this.txtTelefonoP.TabIndex = 5;
            this.txtTelefonoP.Tag = "2";
            this.txtTelefonoP.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtTelefonoP_KeyDown);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(323, 38);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Telefono";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 66);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Ultima Visita";
            // 
            // datePickerUltimaVisita
            // 
            this.datePickerUltimaVisita.Location = new System.Drawing.Point(99, 66);
            this.datePickerUltimaVisita.Name = "datePickerUltimaVisita";
            this.datePickerUltimaVisita.Size = new System.Drawing.Size(200, 20);
            this.datePickerUltimaVisita.TabIndex = 8;
            this.datePickerUltimaVisita.Tag = "3";
            this.datePickerUltimaVisita.ValueChanged += new System.EventHandler(this.datePickerUltimaVisita_ValueChanged);
            // 
            // datePickerUltimaSeduta
            // 
            this.datePickerUltimaSeduta.Enabled = false;
            this.datePickerUltimaSeduta.Location = new System.Drawing.Point(426, 66);
            this.datePickerUltimaSeduta.Name = "datePickerUltimaSeduta";
            this.datePickerUltimaSeduta.Size = new System.Drawing.Size(200, 20);
            this.datePickerUltimaSeduta.TabIndex = 10;
            this.datePickerUltimaSeduta.Tag = "5";
            this.datePickerUltimaSeduta.ValueChanged += new System.EventHandler(this.datePickerUltimaSeduta_ValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(323, 66);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(76, 26);
            this.label5.TabIndex = 9;
            this.label5.Text = "Ultima Seduta \r\nIgene";
            // 
            // cBoxUltimaSeduta
            // 
            this.cBoxUltimaSeduta.AutoSize = true;
            this.cBoxUltimaSeduta.Location = new System.Drawing.Point(405, 65);
            this.cBoxUltimaSeduta.Name = "cBoxUltimaSeduta";
            this.cBoxUltimaSeduta.Size = new System.Drawing.Size(15, 14);
            this.cBoxUltimaSeduta.TabIndex = 11;
            this.cBoxUltimaSeduta.Tag = "4";
            this.cBoxUltimaSeduta.UseVisualStyleBackColor = true;
            this.cBoxUltimaSeduta.CheckedChanged += new System.EventHandler(this.cBoxUltimaSeduta_CheckedChanged);
            // 
            // cBoxOpt
            // 
            this.cBoxOpt.AutoSize = true;
            this.cBoxOpt.Location = new System.Drawing.Point(43, 432);
            this.cBoxOpt.Name = "cBoxOpt";
            this.cBoxOpt.Size = new System.Drawing.Size(15, 14);
            this.cBoxOpt.TabIndex = 12;
            this.cBoxOpt.UseVisualStyleBackColor = true;
            this.cBoxOpt.CheckedChanged += new System.EventHandler(this.cBoxOpt_CheckedChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 432);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(29, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "OPT";
            // 
            // datePickerOpt
            // 
            this.datePickerOpt.Enabled = false;
            this.datePickerOpt.Location = new System.Drawing.Point(64, 432);
            this.datePickerOpt.Name = "datePickerOpt";
            this.datePickerOpt.Size = new System.Drawing.Size(200, 20);
            this.datePickerOpt.TabIndex = 14;
            this.datePickerOpt.Tag = "3";
            this.datePickerOpt.ValueChanged += new System.EventHandler(this.datePickerOpt_ValueChanged);
            // 
            // datePickerTc
            // 
            this.datePickerTc.Enabled = false;
            this.datePickerTc.Location = new System.Drawing.Point(326, 432);
            this.datePickerTc.Name = "datePickerTc";
            this.datePickerTc.Size = new System.Drawing.Size(200, 20);
            this.datePickerTc.TabIndex = 17;
            this.datePickerTc.Tag = "3";
            this.datePickerTc.ValueChanged += new System.EventHandler(this.datePickerTc_ValueChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(278, 432);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(21, 13);
            this.label7.TabIndex = 16;
            this.label7.Text = "TC";
            // 
            // cBoxTc
            // 
            this.cBoxTc.AutoSize = true;
            this.cBoxTc.Location = new System.Drawing.Point(305, 432);
            this.cBoxTc.Name = "cBoxTc";
            this.cBoxTc.Size = new System.Drawing.Size(15, 14);
            this.cBoxTc.TabIndex = 15;
            this.cBoxTc.UseVisualStyleBackColor = true;
            this.cBoxTc.CheckedChanged += new System.EventHandler(this.cBoxTc_CheckedChanged);
            // 
            // datePickerPlacca
            // 
            this.datePickerPlacca.Enabled = false;
            this.datePickerPlacca.Location = new System.Drawing.Point(607, 432);
            this.datePickerPlacca.Name = "datePickerPlacca";
            this.datePickerPlacca.Size = new System.Drawing.Size(200, 20);
            this.datePickerPlacca.TabIndex = 20;
            this.datePickerPlacca.Tag = "3";
            this.datePickerPlacca.ValueChanged += new System.EventHandler(this.datePickerPlacca_ValueChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(532, 432);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(48, 13);
            this.label8.TabIndex = 19;
            this.label8.Text = "PLACCA";
            // 
            // cBoxPlacca
            // 
            this.cBoxPlacca.AutoSize = true;
            this.cBoxPlacca.Location = new System.Drawing.Point(586, 432);
            this.cBoxPlacca.Name = "cBoxPlacca";
            this.cBoxPlacca.Size = new System.Drawing.Size(15, 14);
            this.cBoxPlacca.TabIndex = 18;
            this.cBoxPlacca.UseVisualStyleBackColor = true;
            this.cBoxPlacca.CheckedChanged += new System.EventHandler(this.cBoxPlacca_CheckedChanged);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(820, 465);
            this.Controls.Add(this.datePickerPlacca);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.cBoxPlacca);
            this.Controls.Add(this.datePickerTc);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.cBoxTc);
            this.Controls.Add(this.datePickerOpt);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.cBoxOpt);
            this.Controls.Add(this.cBoxUltimaSeduta);
            this.Controls.Add(this.datePickerUltimaSeduta);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.datePickerUltimaVisita);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtTelefonoP);
            this.Controls.Add(this.txtCognomeP);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtNomeP);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form2";
            this.Text = "Form2";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form2_FormClosed);
            this.Load += new System.EventHandler(this.Form2_Load);
            this.tabControl1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TextBox txtNomeP;
        private System.Windows.Forms.TextBox txtCognomeP;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtTelefonoP;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker datePickerUltimaVisita;
        private System.Windows.Forms.DateTimePicker datePickerUltimaSeduta;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox cBoxUltimaSeduta;
        private System.Windows.Forms.CheckBox cBoxOpt;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker datePickerOpt;
        private System.Windows.Forms.DateTimePicker datePickerTc;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox cBoxTc;
        private System.Windows.Forms.DateTimePicker datePickerPlacca;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox cBoxPlacca;
        private System.Windows.Forms.HelpProvider helpProvider1;
    }
}