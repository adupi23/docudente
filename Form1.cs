﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Odbc;
using System.Configuration;
using System.Collections.Specialized;

namespace DocuDente
{
    public partial class Form1 : Form
    {
        static NameValueCollection appSettings = ConfigurationManager.AppSettings;
        public OdbcConnection connection = null;

        private OdbcConnection connect() {
            OdbcConnection conn = null;

            try
            {
                conn = new OdbcConnection();
                String myConnectionString = "Driver={Microsoft Access Driver (*.mdb)};Dbq=" + appSettings["dataSource"] + ";Uid=Admin;Pwd=" + appSettings["password"] + ";";
                conn.ConnectionString = myConnectionString;
            }
            catch (Exception ex) {
                conn = null;
                MessageBox.Show("ERRORE DI CONNESSIONE:\r\n" + ex.Message);
            }

            return conn;
        }

        public Form1()
        {
            InitializeComponent();
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            connection = connect();
            if (connection != null)
            {
                //riempi tabella
                fillTable(connection, "", "");
            }
        }

        public void fillTable(OdbcConnection conn, String nome, String cognome) {
            String cercaNome = "Select top 20 ID, nome, cognome, telefono From Paziente where nome LIKE '%" + nome + "%' Order By cognome Asc;";
            String cercaCognome = "Select top 20 ID, nome, cognome, telefono From Paziente where cognome LIKE '%" + cognome + "%' Order By cognome Asc;";
            String cerca = "Select top 20 ID, nome, cognome, telefono From Paziente where nome LIKE '%" + nome + "%' And cognome Like '%" + cognome + "%' Order By cognome Asc;";
            String selectPazienti = "Select top 20 ID, nome, cognome, telefono From Paziente Order By data_creazione Desc;";
            String query = "";

            if (nome == "" && cognome == "")
            {
                query = selectPazienti;
            }
            else if (nome != "" && cognome == "")
            {
                query = cercaNome;
            }
            else if (nome == "" && cognome != "")
            {
                query = cercaCognome;
            }
            else
            {
                query = cerca;
            }

            if (conn != null)
            {
                try
                {
                    OdbcCommand select = new OdbcCommand(query, conn);
                    if (conn.State == ConnectionState.Open)
                    {
                        conn.Close();
                    }
                    conn.Open();
                    DataTable dtRecord = new DataTable();
                    dtRecord.Load(select.ExecuteReader());

                    dataGridView1.DataSource = null;
                    dataGridView1.ReadOnly = true;
                    dataGridView1.DataSource = dtRecord;
                    dataGridView1.Columns[0].Visible = false;

                    conn.Close();
                    select.Dispose();

                    txtCercaNome.Clear();
                    txtCercaCognome.Clear();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ERRORE SELEZIONE PAZIENTI: \r\n" + ex.Message);
                }
            }
        }

        private void btnCerca_Click(object sender, EventArgs e)
        {
            fillTable(connection, txtCercaNome.Text, txtCercaCognome.Text);
        }

        private void txtCercaNome_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) {
                fillTable(connection, txtCercaNome.Text, txtCercaCognome.Text);
            }
        }

        private void txtCercaCognome_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                fillTable(connection, txtCercaNome.Text, txtCercaCognome.Text);
            }
        }

        public Paziente p;
        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            int index = e.RowIndex;
            //MessageBox.Show(dataGridView1.Rows[index].Cells[0].Value.ToString());

            Form formPaziente = new Form2(this);
            formPaziente.Text = dataGridView1.Rows[index].Cells[1].Value.ToString() + " " + dataGridView1.Rows[index].Cells[2].Value.ToString();

            p = creaPaziente(connection, Convert.ToInt32(dataGridView1.Rows[index].Cells[0].Value));
            if (p != null)
            {
                formPaziente.ShowDialog(this);
            }
        }

        private Paziente creaPaziente(OdbcConnection conn, int idPaziente) {
            Paziente p = null;
            if (conn != null && idPaziente >= 0)
            {
                String query = "Select * From Paziente Where ID = " + idPaziente;
                try
                {
                    OdbcCommand select = new OdbcCommand(query, conn);
                    if (conn.State == ConnectionState.Open)
                    {
                        conn.Close();
                    }
                    conn.Open();
                    OdbcDataReader reader = select.ExecuteReader();

                    if (!reader.HasRows)
                    {
                        String queryInsert = "Insert into Paziente(nome, cognome, telefono) Values('','','')";
                        String query2 = "Select @@Identity";
                        OdbcCommand insert = new OdbcCommand(queryInsert, conn);
                        if (conn.State == ConnectionState.Open)
                        {
                            conn.Close();
                        }
                        conn.Open();
                        insert.ExecuteNonQuery();
                        insert.CommandText = query2;
                        idPaziente = (int)insert.ExecuteScalar();
                        query = "Select * From Paziente Where ID = " + idPaziente;
                        select.CommandText = query;

                        reader = select.ExecuteReader();
                    }
                    //else {
                    //    MessageBox.Show("Paziente non trovato");
                    //}

                    while (reader.Read())
                    {
                        p = new Paziente(Convert.ToInt32(reader["ID"]), reader["nome"].ToString(), reader["cognome"].ToString(),
                                        reader["telefono"].ToString(), Convert.ToDateTime(reader["ultima_visita"]),
                                        Convert.ToBoolean(reader["ultima_seduta_igene"]),
                                        Convert.ToDateTime(reader["ultima_seduta_igene_data"]), Convert.ToBoolean(reader["opt"]),
                                        Convert.ToDateTime(reader["opt_data"]), Convert.ToBoolean(reader["tc"]),
                                        Convert.ToDateTime(reader["tc_data"]), Convert.ToBoolean(reader["placca"]),
                                        Convert.ToDateTime(reader["placca_data"]), Convert.ToDateTime(reader["ultima_modifica"]));
                        break;
                    }

                    conn.Close();
                    select.Dispose();
                }
                catch (Exception ex)
                {
                    conn.Close();
                    p = null;
                    MessageBox.Show("ERRORE SELEZIONE PAZIENTI: \r\n" + ex.Message);
                }
            }
            return p;
        }

        private void aggiungiToolStripMenuItem_Click(object sender, EventArgs e)
        {

            Form formPaziente = new Form2(this);
            formPaziente.Text = "";

            p = creaPaziente(connection, 0);
            if (p != null)
            {
                formPaziente.ShowDialog(this);
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
