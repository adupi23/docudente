﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocuDente
{
    public class Paziente
    {
        public int Id { get; set; }
        public String Nome { get; set; }
        public String Cognome { get; set; }
        public String Telefono { get; set; }
        public DateTime UltimaVisita { get; set; }
        public Boolean UltimaSedutaIgene { get; set; }
        public DateTime UltimaSedutaIgeneData { get; set; }
        public Boolean Opt { get; set; }
        public DateTime OptData { get; set; }
        public Boolean Tc { get; set; }
        public DateTime TcData { get; set; }
        public Boolean Placca { get; set; }
        public DateTime PlaccaData { get; set; }
        public DateTime UltimaModifica { get; set; }

        public Paziente(int _id, String _nome, String _cognome, String _telefono, DateTime _ultima_visita, Boolean _ultima_seduta_igene,
                        DateTime _ultima_seduta_igene_data, Boolean _opt, DateTime _opt_data, Boolean _tc, DateTime _tc_data,
                        Boolean _placca, DateTime _placca_data, DateTime _ultima_modifica) {
            this.Id = _id;
            this.Nome = _nome;
            this.Cognome = _cognome;
            this.Telefono = _telefono;
            this.UltimaVisita = _ultima_visita;
            this.UltimaSedutaIgene = _ultima_seduta_igene;
            this.UltimaSedutaIgeneData = _ultima_seduta_igene_data;
            this.Opt = _opt;
            this.OptData = _opt_data;
            this.Tc = _tc;
            this.TcData = _tc_data;
            this.Placca = _placca;
            this.PlaccaData = _placca_data;
            this.UltimaModifica = _ultima_modifica;
        }
    }
}
